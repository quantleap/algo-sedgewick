package princeton.algo.ch1.exercises;

import org.junit.Test;

/**
 * Iterative Fibonacci
 * @author fernando
 */
public class Ex_1_1_SimpleTest {

	@Test
	public void ex1_1_6_IterativeFibonacci() throws Exception {
		int f = 0;
		int g = 1;
		for (int i = 0; i <= 15; i++){
			System.out.println(f);
			f = f + g;
			g = f - g;
		}
	}
	
	@Test
	public void ex1_1_7a() throws Exception {
		System.out.println("--------------------1.1.7a------------");
		double t = 9.0;
		while (Math.abs(t - 9.0/t) > .001)
			t = (9.0/t + t) / 2.0;
		System.out.printf("%.5f\n", t);
	}
	
	/**
	 * X^2 /2
	 * @throws Exception
	 */
	@Test
	public void ex1_1_7b() throws Exception {
		System.out.println("--------------------1.1.7b------------");

		int n = 1000;
		int sum = 0;
		for (int i = 1; i < n; i++)
			for (int j = 0; j < i; j++)
				sum++;
		System.out.println(sum);
	}
	
}