package princeton.algo.ch1.work;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class StackCalculatorTest {

	private StackCalculator stackCalculator;

	@Before
	public void setup(){
		stackCalculator = new StackCalculator();
	}

	@Test(expected=IllegalArgumentException.class)
	public void requireAlwaysEmptySpaces() {
		stackCalculator.evaluate("(1+1)");
	}	
	
	@Test(expected=IllegalArgumentException.class)
	public void requireMinimumExpressions() {
		stackCalculator.evaluate("( 1 + 1");
	}	
	
	@Test
	public void requireAlwaysParenthesis() {
		assertEquals(2d, stackCalculator.evaluate("( 1 + 1 )"),0.0001);
	}	
	
	@Test
	public void evaluateComplexEpression1() {
		assertEquals(101d, stackCalculator.evaluate("( 1 + ( ( 2 + 3 ) * ( 4 * 5 ) ) )"), 0.0001);
	}

	@Test
	public void evaluateComplexEpression2() {
		assertEquals(1.618033988749895, stackCalculator.evaluate("( ( 1 + sqrt ( 5.0 ) ) / 2.0 )"), 0.00000001);
	}
	
	@Test(expected=EnumConstantNotPresentException.class)
	public void unsupportedOperation() {
		stackCalculator.evaluate("( 1 % 1 )");
	}	
	
}