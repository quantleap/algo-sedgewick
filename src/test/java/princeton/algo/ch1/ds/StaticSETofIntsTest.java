package princeton.algo.ch1.ds;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StaticSETofIntsTest {

	private StaticSETofInts testObject;

	@Test
	public void doesNotConain() {
		testObject = new StaticSETofInts(new int[]{0,5,2,4,7,8,4,16,3,38,22,51});
		
		assertFalse(testObject.contains(17));
	}
	
	
	@Test
	public void doesContain() {
		testObject = new StaticSETofInts(new int[]{0,5,2,4,7,8,4,16,3,38,22,51});
		
		assertTrue(testObject.contains(22));
	}

}
