package princeton.algo.ch1.ds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Iterator;

import org.junit.Test;

public class DynamicArrayStackTest {

	@Test
	public void pushStrings() {
		final String dt = "Dream Theater";
		final String megadeth = "Megadeth";
		final String symphonyX = "Symphony X";
		final DynamicArrayStack<String> testObject = new DynamicArrayStack<String>(5);
		testObject.push(symphonyX);
		testObject.push(megadeth);
		testObject.push(dt);
		
		assertEquals(3, testObject.size());
		assertEquals(dt, testObject.pop());
		assertEquals(megadeth, testObject.pop());
		assertEquals(symphonyX, testObject.pop());
		assertEquals(0, testObject.size());
	}

	@Test
	public void pushIntegersAndResize() {
		final DynamicArrayStack<Integer> testObject = new DynamicArrayStack<Integer>(5);
		for(int i = 1; i <= 11; i++){
			testObject.push(i);
		}
		
		assertEquals(11, testObject.size());
		assertEquals(Integer.valueOf(11), testObject.pop());
		assertEquals(10, testObject.size());
	}
	
	@Test
	public void constructorInit() {
		final DynamicArrayStack<Integer> testObject = new DynamicArrayStack<Integer>(new Integer[]{2,5,6,7,7,8,8,5,19});
		assertEquals(9, testObject.size());
		assertEquals(Integer.valueOf(19), testObject.pop());
		assertFalse(testObject.isEmpty());
		final Iterator<Integer> iterator = testObject.iterator();
		assertEquals(Integer.valueOf(5), iterator.next());
		assertEquals(Integer.valueOf(8), iterator.next());
	}

}
