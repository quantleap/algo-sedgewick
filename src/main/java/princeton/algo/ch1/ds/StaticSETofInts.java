package princeton.algo.ch1.ds;

/*************************************************************************
 *  Compilation:  javac StaticSetOfInts.java
 *  
 *  Data type to store a set of integers.
 *
 *************************************************************************/

import java.util.Arrays;

public class StaticSETofInts {
    private int[] items;
    public StaticSETofInts(int[] keys) {
        // defensive copy
        items = new int[keys.length];
        for (int i = 0; i < keys.length; i++)
            items[i] = keys[i];

        Arrays.sort(items);

        // probably should check that no duplicates
    }

    public boolean contains(int key) {
        return rank(key) != -1;
    }

    /**
     * binary search
     * @param value
     * @return
     */
	public int rank(int value) {
		int low = 0;
		int high = items.length - 1;
		while(low <= high) {
			// value is in a[low..high] or not present.
			int mid =  low + (high - low) / 2;
			if		(value < items[mid]) high = mid - 1;
			else if (value > items[mid]) low  = mid + 1;
			else return mid;
		}
		return -1;
	}

    // Binary search.
//  public int rank(int key) {
//      int lo = 0;
//      int hi = a.length - 1;
//      while (lo <= hi) {
//          // Key is in a[lo..hi] or not present.
//          int mid = lo + (hi - lo) / 2;
//          if      (key < a[mid]) hi = mid - 1;
//          else if (key > a[mid]) lo = mid + 1;
//          else return mid;
//      }
//      return -1;
//  }

}
